package controller;

import java.io.File;

import model.exceptions.TripNotFoundException;
import model.logic.STSManager;
import api.ISTSManager;

public class Controller {
	
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void loadStops() {
		manager.loadStops();		
	}

	public static void readBusUpdates() {
		File f = new File("data");
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			manager.readBusUpdate(updateFiles[i]);
		}
	}
	
	public static void listStops(Integer tripId) throws TripNotFoundException{
		throw new TripNotFoundException();
	}
	

}
