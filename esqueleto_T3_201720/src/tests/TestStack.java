package tests;

import static org.junit.Assert.*;
import model.data_structures.Node;
import model.data_structures.Queue;

import org.junit.Test;
public class TestStack {

	@Test
	public void test()
	{
		testSize();
		testAgregar();	
		testDequeue();
		
	}
	
	public Queue<Node<String>> cola;
	
	public void caso1()
	{
		cola =  new Queue<Node<String>>();
	}
	
	public void caso2()
	{
		cola = new Queue<Node<String>>();
		cola.enqueue(new Node("Nodo 1"));
		cola.enqueue(new Node("Nodo 2"));
		cola.enqueue(new Node("Nodo 3"));
		cola.enqueue(new Node("Nodo 4"));
		cola.enqueue(new Node("Nodo 5"));

	}
	
	
	
	@Test
	public void testSize()
	{
		caso2();
		assert 5 == cola.getSize() : "El tamanho de la lista no es el esperado";
	}


	@Test
	public void testAgregar() 
	{
		caso1();
		cola.enqueue(new Node<>("Primer nodo agregado"));
		try
		{
			assert("Primer nodo agregado".equals(cola.dequeue().giveInfo()));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
				
	}

	
	@Test
	public void testDequeue() 
	{
		caso2();
		try
		{
			assert(cola.getPrimeroEnSalir().giveInfo().equals("Nodo 1")): "Hay un error con el queue";
			cola.dequeue();
			assert(cola.getPrimeroEnSalir().giveInfo().equals("Nodo 2")): "Hay un error con el dequeue";
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		
	}


}