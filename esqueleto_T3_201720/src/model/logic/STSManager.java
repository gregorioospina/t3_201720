package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import model.data_structures.IStack;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.BusUpdateVO;
import model.vo.VOStop;
import api.ISTSManager;

public class STSManager<E> implements ISTSManager{

	private JsonParser json;
	public Queue<BusUpdateVO> colaBus = new Queue<BusUpdateVO>();
	private Queue Stops;
	private Queue tripIds;
	File[] files = new File("").listFiles();



	public void readBusUpdate(File rtFile) {

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(rtFile));
			Gson gson = new GsonBuilder().create();
			BusUpdateVO [] bus= gson.fromJson(reader, BusUpdateVO[].class);

			for (int i = 0; i < bus.length; i++) {
				colaBus.enqueue(bus[i]);
				System.out.println(bus[i].getRecordedTime());
			}

		}
		catch(FileNotFoundException e){

		}
		catch(Exception e){

		}
		finally
		{
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	public IStack<VOStop> listStops(Integer tripID) {

		Stack<VOStop> stacco = new Stack<VOStop>();
		tripIds = new Queue<BusUpdateVO>();
		Node<BusUpdateVO> nodox = colaBus.getUltimoEnEntrar();
		while(nodox.darNext() != null)
		{
			if(Integer.parseInt(nodox.giveInfo().getTripID()) == tripID)
			{
				tripIds.enqueue(nodox);
			}
			nodox = nodox.darNext();
		}

		for (int i = 0; i < tripIds.getSize(); i++) {

			Node<BusUpdateVO> carlos  = tripIds.getUltimoEnEntrar();
			for (int j = 0; j < Stops.getSize(); j++) {
				Node<VOStop> manuel = Stops.getUltimoEnEntrar();

				if(getDistance(carlos.giveInfo().getLatitude(), carlos.giveInfo().getLongitud(), manuel.giveInfo().getStopLat(), manuel.giveInfo().getStopLon()) <= 70.0)
				{
					stacco.push(manuel.giveInfo());
				}


			}
		}




		return stacco;
	}


	public void loadBuses(File[] files) {
		for (File file : files) {
			if (file.getName().equals("stops.txt")) 
			{}
			else 
			{
				readBusUpdate(file);
			}

		}
	}


	@Override
	public void loadStops() 
	{

		String linea;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try 
		{

			BufferedReader br = new BufferedReader(new FileReader("data/stops.txt"));
			Stops = new Queue<VOStop>();
			br.readLine();
			while( (linea = br.readLine()) != null)
			{

				String[] lineas = linea.split(splitBy);
				int stopID = Integer.parseInt((lineas[0]));
				String stopCode = lineas[1];
				String stopName = lineas[2];
				String stopDesc = lineas[3];
				double stopLat = Double.parseDouble(lineas[4]);
				double stopLon = Double.parseDouble(lineas[5]);
				String zoneID =lineas[6];
				String stopURL = lineas[7];
				String locationType = lineas[8];

				VOStop vor = new VOStop(stopID, stopCode, stopName, stopDesc, stopLat, stopLon, zoneID, stopURL, locationType);
				Stops.enqueue(vor);
			}

			//System.out.println(Stops.Size());
			br.close();

		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		

	}

	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;

	}

	private Double toRad(Double value) {
		return value * Math.PI / 180;
	}



}
