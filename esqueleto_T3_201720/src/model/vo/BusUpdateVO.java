package model.vo;

import com.google.gson.JsonElement;

public class BusUpdateVO {

	private String vehicleNo;
	private String tripID;
	private String routeNo;
	private String direction;
	private String destination;
	private String pattern;
	private double latitude;
	private double longitud;
	private String recordedTime;
	private String routeMap;
	/*
	 * href
	 */
	
	public BusUpdateVO(String vehicleNo2, String tripID2, String routeNo2, String direction2, String destination2, String pattern2, double longitud2,double latitude2, String recordedTime2, String routeMap2)
	{
		vehicleNo = vehicleNo2;
		tripID = tripID2;
		routeNo = routeNo2;
		direction = direction2;
		destination = destination2;
		pattern = pattern2;
		latitude = latitude2;
		longitud = longitud2;
		recordedTime = recordedTime2;
		routeMap = routeMap2;
		
	}
	
	
	public String getVehicleNo() {
		return vehicleNo;
	}



	public String getTripID() {
		return tripID;
	}


	public String getDirection() {
		return direction;
	}




	public String getDestination() {
		return destination;
	}



	public String getPattern() {
		return pattern;
	}


	public double getLatitude() {
		return latitude;
	}


	public double getLongitud() {
		return longitud;
	}

	public String getRecordedTime() {
		return recordedTime;
	}


	public String getRouteMap() {
		return routeMap;
	}
	
	


	
}
