package model.data_structures;

public class Node<T> {

	
	private Node<T> next;
	private T data;
	
	
	public Node(T datax)
	{
		this.data = datax;
		next = null;
	}
	
	public Node(T initialData, Node<T> newNext)
	{
		this.data = initialData;
		this.next = newNext;
	}
	
	public String toString(){
		return data + "";
	}
	
	public void nuevoNodoDespues(T aAgregar)
	{
		this.next = new Node<T> (aAgregar, next);
		
	}
	
	public Node<T> darNext()
	{
		return next;
	}
	
	
	public void cambiarNext(Node<T> nuevoNext)
	{
		this.next = nuevoNext;
	}
	
	
	public T giveInfo()
	{
		return data;
	}
	
	
	
	
			
}